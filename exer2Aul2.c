#include <stdio.h>
#include <stdlib.h>


struct Numeros {
  int inteiro;
  char literal[20];
};


void imprime(struct Numeros *, int);
void troca(struct Numeros *, int, int);
void bubbleSort(struct Numeros *,int);



int main(int argc, char **argv)
{
	int tam,j;
	struct Numeros *arr;
	
	scanf("%d",&tam);
	
	
	
	arr = malloc(tam*sizeof(struct Numeros));
	if (arr == NULL) {
		perror("Error");
		return EXIT_FAILURE;
		}
	
    
    
    for(j=0;j<tam;j++){
		
		scanf("%d %s", &(arr[j].inteiro), arr[j].literal);
		
		
		
		}
		
	
	
	printf("Tamanho do vetor: %d\n",tam);
	
	
	bubbleSort(arr,tam);	
	imprime(arr,tam);
	
	return 0;

}

void imprime(struct Numeros *arr, int tam){
	int j;
	
	for(j=0;j<tam-1;j++){
					
				
			printf("%d ", arr[j].inteiro);
			printf("%s\n", arr[j].literal);
		
		}
	printf("%d ", arr[j].inteiro);
	printf("%s\n", arr[j].literal);

	
}




void troca(struct Numeros arr[] , int k, int l){
	
	struct Numeros aux;
	
	aux = arr[k];
	arr[k] = arr[l];
	arr[l] = aux;
	
	
	
}


void bubbleSort(struct Numeros *arr, int tam){
	int l,m;
	
	
	for(l=0;l<tam;l++){
		for(m=tam-1;m>l;m--){
			
			if ( arr[m].inteiro<arr[m-1].inteiro){
				troca(arr,m,m-1);
			}	
			
			}
		
		}
	
	
}

